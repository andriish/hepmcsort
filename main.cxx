#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"
#include "HepMC/GenParticle.h"

using namespace HepMC;


bool compare(const GenParticle& a, const GenParticle& b)  {
    if (a.pdg_id() != b.pdg_id()) return a.pdg_id() > b.pdg_id();
    if (a.status() != b.status()) return a.status() > b.status();
    return a.momentum().pz() > b.momentum().pz();
}
class Ordering {

public:

    Ordering() { }

private:
    std::map< GenParticle*, int >  p_paths_map;
    std::map< GenVertex*, int >    v_paths_map;
    std::vector< std::pair<GenParticle*, int> >  p_paths_vector;
    std::vector< std::pair<GenVertex*, int> >    v_paths_vector;

    int get_path_length(GenParticle* p);
    int get_path_length(GenVertex* v);

    struct {
        bool operator()(const std::pair<GenParticle*, int> a,const  std::pair<GenParticle*, int> b) const {
            if  (a.second != b.second) return (a.second < b.second);
            return compare(*a.first,*b.first);
        }
    } less_genparticle_path;

    struct {
        bool operator()(const std::pair<GenVertex*, int> a,const  std::pair<GenVertex*, int> b) const {
            if  (a.second != b.second) return a.second < b.second;
            if (a.first->particles_out_size()!=b.first->particles_out_size()) return a.first->particles_out_size()< b.first->particles_out_size();
            return  compare(**a.first->particles_begin(HepMC::children),**b.first->particles_begin(HepMC::children));
        }
    } less_genvertex_path;

public:
    GenEvent* reshuffle(GenEvent* e) {
        p_paths_map.clear();
        v_paths_map.clear();
        p_paths_vector.clear();
        v_paths_vector.clear();

        for (auto p = e->particles_begin(); p != e->particles_end(); ++p) if (*p) get_path_length(*p);

        for (auto v = e->vertices_begin(); v != e->vertices_end(); ++v) if (*v) get_path_length(*v);
        for (auto p: p_paths_map) p_paths_vector.push_back(p);
        for (auto v: v_paths_map) v_paths_vector.push_back(v);
        std::sort(p_paths_vector.begin(), p_paths_vector.end(), less_genparticle_path);
        std::sort(v_paths_vector.begin(), v_paths_vector.end(), less_genvertex_path);
        for (int i = 0; i < p_paths_vector.size(); i++) {
            auto x = p_paths_vector[i].first->suggest_barcode(p_paths_vector.size()+i+1);
            if (!x) printf("BAD PARTICLE  %i\n",i);
        }
        for (int i = 0; i < p_paths_vector.size(); i++) {
            auto x=p_paths_vector[i].first->suggest_barcode(i+1);
            if (!x) printf("BAD PARTICLE  %i\n",i);
        }
        for (int i = 0; i < v_paths_vector.size(); i++) {
            auto x = v_paths_vector[i].first->suggest_barcode(-i-1);
            if (!x) printf("BAD VERTEX %i\n",i);
        }
        return e;
    }
};

int Ordering::get_path_length(GenParticle* p) {
    if (p_paths_map.count(p)) return p_paths_map.at(p);
    auto v = p->production_vertex();
    int r = v ? get_path_length(v): 0;
    p_paths_map[p] = r;
    return r;
}
int Ordering::get_path_length(GenVertex* v) {
    if (v_paths_map.count(v)) return v_paths_map.at(v);
    GenParticle* p = v->particles_in_size()? *(v->particles_begin(HepMC::parents)) : nullptr;
    int r = p ? get_path_length(p) + 1 : 0;
    v_paths_map[v] = r;
    return r;
}



int main(int argc,char** argv) {
    HepMC::IO_GenEvent input(argv[1],std::ios::in);
    HepMC::IO_GenEvent output(argv[2],std::ios::out);
    int icount=0;
    HepMC::GenEvent* evt = input.read_next_event();
    while ( evt ) {
        Ordering A;
        A.reshuffle(evt);
        output << evt;
        delete evt;
        evt = input.read_next_event();
    }
    if (evt) delete evt;
    return 0;
}
